/*
 * dht_client 
 *  
 * main.c 
 * 
 * Adapted from Zephyr echo_client
 * 	
 */

#include <logging/log.h>
LOG_MODULE_REGISTER(dht11_client_sample, LOG_LEVEL_DBG);

#include <zephyr.h>
#include <errno.h>
#include <stdio.h>

#include <net/net_pkt.h>
#include <net/net_core.h>
#include <net/net_context.h>

#include <net/net_app.h>

#include <sensor.h>
#include <device.h>
#include <gpio.h>

static struct device *dev;

#include "common.h"

#define APP_BANNER "DHT11 client"



struct configs conf = {
	.ipv4 = {
		.proto = "IPv4",
	},
	.ipv6 = {
		.proto = "IPv6",
	},
};

static struct k_sem quit_lock;
struct k_sem tcp_ready;

void panic(const char *msg)
{
	if (msg) {
		LOG_ERR("%s", msg);
	}

	k_sem_give(&quit_lock);

	for (;;) {
		k_sleep(K_FOREVER);
	}
}

static inline int init_app(void)
{
	LOG_INF(APP_BANNER);

	k_sem_init(&quit_lock, 0, UINT_MAX);

	init_vlan();

	return 0;
}

char data[13];

static void get_sensor_data()
{
	struct sensor_value temp, humidity;
	sensor_sample_fetch(dev);

	sensor_channel_get(dev, SENSOR_CHAN_AMBIENT_TEMP, &temp);
	sensor_channel_get(dev, SENSOR_CHAN_HUMIDITY, &humidity);

	printk("temp: %d.%06d; humidity: %d.%06d\n", temp.val1,
			temp.val2, humidity.val1, humidity.val2);
			
	char strData[100];
	sprintf(strData, "1:%d.%d:%d.%d",
             temp.val1, temp.val2/100000, 
             humidity.val1, humidity.val2/100000);
	strncpy(data, strData, 13);
}


void main(void)
{
	int ret;

	init_app();

	dev = device_get_binding("DHT11");

	if (IS_ENABLED(CONFIG_NET_UDP)) {
		start_udp();
	}


	while (1) {

		get_sensor_data();

		send_data();

		k_sleep(5000);

	}

	k_sem_take(&quit_lock, K_FOREVER);

	LOG_INF("Stopping...");

	if (IS_ENABLED(CONFIG_NET_UDP)) {
		stop_udp();
	}
}
