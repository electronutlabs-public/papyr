/** @file module.c
 *
 * @brief A description of the module’s purpose. 
 *
 * @par
 * COPYRIGHT NOTICE: (c) 2018 Electronut Labs.
 * All rights reserved. 
*/
 
#include <string.h>

#include "common.h"
#include "epaper.h"

#include "epaper_driver/epd1in54b.h"
#include "epaper_driver/epdif.h"
#include "epaper_driver/epdpaint.h"
#include "epaper_driver/image.h"
#include "epaper_driver/fonts.h"

EPD epd;

unsigned char frame_buffer_black_arr[EPD_WIDTH * EPD_HEIGHT / 8];
unsigned char *frame_buffer_black = frame_buffer_black_arr;
unsigned char frame_buffer_red_arr[EPD_WIDTH * EPD_HEIGHT / 8];
unsigned char *frame_buffer_red = frame_buffer_red_arr;

Paint paint_black;
Paint paint_red;

int epaper_init()
{
    epaper_GPIO_Init();
    // spi_epd_init();
    
    return EPD_Init(&epd);
}

void epaper_test(th_data data[])
{

    Paint_Init(&paint_black, frame_buffer_black, epd.width, epd.height);
    Paint_Init(&paint_red, frame_buffer_red, epd.width, epd.height);
    Paint_Clear(&paint_black, UNCOLORED);
    Paint_Clear(&paint_red, UNCOLORED);

    Paint_SetRotate(&paint_black, 3);
    Paint_SetRotate(&paint_red, 3);

    Paint_DrawFilledRectangle(&paint_black, 0, 0, 199, 99, COLORED);
    Paint_DrawFilledRectangle(&paint_red, 0, 100, 199, 199, COLORED);

    
    Paint_DrawStringAt(&paint_black, 10, 60, data[0].T, &Font24, UNCOLORED);
    Paint_DrawStringAt(&paint_black, 110, 10, data[0].H, &Font24, UNCOLORED);
    
    Paint_DrawStringAt(&paint_red, 10, 160, data[1].T, &Font24, UNCOLORED);
    Paint_DrawStringAt(&paint_red, 110, 110, data[1].H, &Font24, UNCOLORED);

    /* Display the frame_buffer */
    #if 0
    Paint_SetRotate(&paint_black, 3);
    Paint_DrawStringAt(&paint_black, 10, 50, temp, &Font24, UNCOLORED);
    #endif

     #if 0

    Paint_DrawBitmap(&paint_black, 0, 0, zephyr_bwData, zephyr_image_width, zephyr_image_height, COLORED);
    
    Paint_SetRotate(&paint_red, 3);
    Paint_DrawStringAt(&paint_red, 10, 160, "Hello Zephyr!", &Font20, COLORED);

   
    Paint_DrawRectangle(&paint_black, 10, 60, 50, 110, COLORED);
    Paint_DrawLine(&paint_black, 10, 60, 50, 110, COLORED);
    Paint_DrawLine(&paint_black, 50, 60, 10, 110, COLORED);
    Paint_DrawLine(&paint_red, 50, 60, 10, 110, COLORED);

    Paint_DrawCircle(&paint_red, 120, 80, 30, COLORED);

    Paint_DrawFilledRectangle(&paint_black, 10, 130, 50, 180, COLORED);
    Paint_DrawFilledRectangle(&paint_red, 10, 130, 50, 180, COLORED);

    Paint_DrawFilledCircle(&paint_red, 120, 150, 30, COLORED);
    Paint_DrawFilledCircle(&paint_red, 120, 150, 25, UNCOLORED);

    /*Write strings to the buffer */
    Paint_SetRotate(&paint_black, 3);
    Paint_DrawStringAt(&paint_black, 0, 180, "EPAPER TEST", &Font20, COLORED);
    Paint_SetRotate(&paint_black, 0);
    Paint_DrawStringAt(&paint_red, 0, 0, "Hello world!", &Font20, COLORED);
    Paint_DrawStringAt(&paint_black, 0, 0, "Hello world!", &Font20, COLORED);

    Paint_DrawPixel(&paint_black, 50, 50, COLORED);
    Paint_DrawPixel(&paint_black, 50, 51, COLORED);
    Paint_DrawPixel(&paint_black, 50, 52, COLORED);
    Paint_DrawPixel(&paint_black, 50, 53, COLORED);
    Paint_DrawPixel(&paint_black, 50, 54, COLORED);
    Paint_DrawPixel(&paint_black, 50, 55, COLORED);
    Paint_DrawPixel(&paint_black, 50, 56, COLORED);
    Paint_DrawPixel(&paint_black, 50, 57, COLORED);
    Paint_DrawPixel(&paint_black, 50, 58, COLORED);
    #endif
    
    EPD_DisplayFrame(&epd, frame_buffer_black, frame_buffer_red);
}

void epaper_clear()
{
    Paint_Init(&paint_black, frame_buffer_black, epd.width, epd.height);
    Paint_Init(&paint_red, frame_buffer_red, epd.width, epd.height);
    Paint_Clear(&paint_black, UNCOLORED);
    Paint_Clear(&paint_red, UNCOLORED);
    EPD_DisplayFrame(&epd, frame_buffer_black, frame_buffer_red);
}