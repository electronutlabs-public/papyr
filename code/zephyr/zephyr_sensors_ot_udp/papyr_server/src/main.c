/* echo.c - Networking echo server */

/*
 * Copyright (c) 2016 Intel Corporation.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <logging/log.h>
LOG_MODULE_REGISTER(papyr_server_sample, LOG_LEVEL_DBG);

#include <zephyr.h>
#include <linker/sections.h>
#include <errno.h>

#include <net/net_pkt.h>
#include <net/net_core.h>
#include <net/net_context.h>
#include <net/net_app.h>

#include <device.h>
#include <gpio.h>
#include <spi.h>
#include "epaper.h"


#include "common.h"

/* The startup time needs to be longish if DHCP is enabled as setting
 * DHCP up takes some time.
 */
#define APP_STARTUP_TIME K_SECONDS(20)

#define BUF_TIMEOUT K_MSEC(100)

#define APP_BANNER "Run echo server"

static struct device * port0;

static struct k_sem quit_lock;

struct k_sem update_sem;

// current received data 
th_data curr_th_data[2];

void panic(const char *msg)
{
	if (msg) {
		LOG_ERR("%s", msg);
	}

	for (;;) {
		k_sleep(K_FOREVER);
	}
}

void quit(void)
{
	k_sem_give(&quit_lock);
}

struct net_pkt *build_reply_pkt(const char *name,
				struct net_app_ctx *ctx,
				struct net_pkt *pkt,
				u8_t proto_len)
{
	struct net_pkt *reply_pkt;
	struct net_buf *frag;
	int header_len = 0;
	int recv_len;
	int ret;

	LOG_INF("%s received %d bytes", name, net_pkt_appdatalen(pkt));
	LOG_INF("%s received data: %16s", name, net_pkt_appdata(pkt));

	if (net_pkt_appdatalen(pkt) == 0) {
		return NULL;
	}

	reply_pkt = net_app_get_net_pkt(ctx, net_pkt_family(pkt), BUF_TIMEOUT);
	if (!reply_pkt) {
		return NULL;
	}

	NET_ASSERT(net_pkt_family(reply_pkt) == net_pkt_family(pkt));

	recv_len = net_pkt_get_len(pkt);

	/* If we have link layer headers, then get rid of them here. */
	if (recv_len != net_pkt_appdatalen(pkt)) {
		/* First fragment will contain IP header so move the data
		 * down in order to get rid of it.
		 */
		header_len = net_pkt_ip_hdr_len(pkt);
		header_len += net_pkt_ipv6_ext_len(pkt);
		header_len += proto_len;

		ret = net_pkt_pull(pkt, 0, header_len);
		if (ret != 0) {
			net_pkt_unref(reply_pkt);
			return NULL;
		}
	}

	net_pkt_set_appdatalen(reply_pkt, net_pkt_appdatalen(pkt));

	frag = net_pkt_copy_all(pkt, 0, BUF_TIMEOUT);
	if (!frag) {
		LOG_ERR("Failed to copy all data");
		net_pkt_unref(reply_pkt);
		return NULL;
	}

	net_pkt_frag_add(reply_pkt, frag);

	return reply_pkt;
}

void pkt_sent(struct net_app_ctx *ctx,
	     int status,
	     void *user_data_send,
	     void *user_data)
{
	if (!status) {
		LOG_INF("Sent %d bytes", POINTER_TO_UINT(user_data_send));
	}
}

static inline int init_app(void)
{
	k_sem_init(&quit_lock, 0, UINT_MAX);

	k_sem_init(&update_sem, 0, UINT_MAX);

	
	init_vlan();

	return 0;
}

void main(void)
{
	port0 = device_get_binding("GPIO_0");

	/* Set LED pin as output */
    gpio_pin_configure(port0, 13, GPIO_DIR_OUT);
	gpio_pin_configure(port0, 14, GPIO_DIR_OUT);
    gpio_pin_configure(port0, 15, GPIO_DIR_OUT);


    // turn on epaper display MOSFET
    gpio_pin_configure(port0, 11, GPIO_DIR_OUT);
    gpio_pin_write(port0, 11, 0);

    gpio_pin_write(port0, 13, 1);
    gpio_pin_write(port0, 14, 1);
    gpio_pin_write(port0, 15, 1);

    // init epaper display
    epaper_init();

	epaper_test(curr_th_data);

	init_app();

	if (IS_ENABLED(CONFIG_NET_UDP)) {
		start_udp();
	}

	s64_t ts;
	s64_t elapsed_ms;
	// initial time stamp
	ts = k_uptime_get();
	k_sleep(500);
	elapsed_ms = k_uptime_delta(&ts);
	printk(">>elapsed: %lld\r\n", elapsed_ms);

	while (1) {

		k_sem_take(&update_sem, K_FOREVER);
		printk("got sem...\r\n");

		// has enough time passed?
		elapsed_ms += k_uptime_delta(&ts);
		printk("elapsed: %lld\r\n", elapsed_ms);
		if (elapsed_ms > 20000) {
			printk("updating display...\r\n");
			epaper_test(curr_th_data);
			elapsed_ms = 0;
			k_uptime_delta(&ts);
		}
	}

	k_sem_take(&quit_lock, K_FOREVER);

	LOG_INF("Stopping...");

	if (IS_ENABLED(CONFIG_NET_UDP)) {
		stop_udp();
	}
}
