/**
 *  @filename   :   epdif.c
 *  @brief      :   Implements EPD interface functions
 *                  Users have to implement all the functions in epdif.cpp
 *  @author     :   Yehui from Waveshare
 *
 *  Copyright (C) Waveshare     July 7 2017
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documnetation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to  whom the Software is
 * furished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS OR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <zephyr.h>
#include <device.h>
#include <gpio.h>
#include <spi.h>
#include "epdif.h"

#define SPI_SLAVE 0

// GPIO
struct device *epaper_gpio;

// SPI
struct device *epaper_spi;

struct spi_cs_control epd_spi_cs_pin = {
	.gpio_pin = EPD_SPI_CS_PIN,
	.delay = 0,
};

struct spi_config epaper_spi_config = {
  .frequency = 268435456, // FAST_FREQ
  .operation = SPI_OP_MODE_MASTER | SPI_WORD_SET(8), // SPI_MODE_CPOL, SPI_MODE_CPHA, SPI_OP_MODE_MASTER
  .slave = SPI_SLAVE,
  .cs = &epd_spi_cs_pin,
};

#if 0 //Mahesh
#include "nrf_delay.h"
#include "nrf_gpio.h"
#include "nrf_drv_spi.h"

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"
#endif

#include "epdif.h"

EPD_Pin epd_cs_pin = {
  EPD_SPI_CS_PIN,
};

EPD_Pin epd_rst_pin = {
  EPD_RST_PIN,
};

EPD_Pin epd_dc_pin = {
  EPD_DC_PIN,
};

EPD_Pin epd_busy_pin = {
  EPD_BUSY_PIN,
};

EPD_Pin pins[4];

void epaper_GPIO_Init(void)
{
  // nrf_gpio_pin_clear(EPD_DC_PIepaper_gpio);
  // nrf_gpio_pin_clear(EPD_RST_Pepaper_gpioN);
  // nrf_gpio_pin_clear(EPD_SPI_Cepaper_gpio_PIN);

  // nrf_gpio_cfg_output(EPD_DC_Pepaper_gpioN);
  // nrf_gpio_cfg_output(EPD_RST_epaper_gpioIN);
  // nrf_gpio_cfg_output(EPD_SPI_epaper_gpioS_PIN);
  // nrf_gpio_cfg_input(EPD_BUSY_epaper_gpioIN, NRF_GPIO_PIN_NOPULL);
  
  epaper_gpio = device_get_binding("GPIO_0");

  if(!epaper_gpio)
  {
    printk("GPIO binding error\r\n");
  }

  gpio_pin_configure(epaper_gpio, EPD_DC_PIN, GPIO_DIR_OUT);
  gpio_pin_configure(epaper_gpio, EPD_RST_PIN, GPIO_DIR_OUT);
  gpio_pin_configure(epaper_gpio, EPD_SPI_CS_PIN, GPIO_DIR_OUT);
  gpio_pin_configure(epaper_gpio, EPD_BUSY_PIN, GPIO_DIR_IN);

  gpio_pin_write(epaper_gpio, EPD_DC_PIN, 0);
  gpio_pin_write(epaper_gpio, EPD_RST_PIN, 0);
  gpio_pin_write(epaper_gpio, EPD_SPI_CS_PIN, 0);
}

#if 0
#define SPI_INSTANCE  2 /**< SPI instance index. */
const nrf_drv_spi_t spi_epd = NRF_DRV_SPI_INSTANCE(SPI_INSTANCE);  /**< SPI instance. */

void spi_epd_init()
{
  nrf_drv_spi_config_t spi_config_epd = NRF_DRV_SPI_DEFAULT_CONFIG;
  spi_config_epd.frequency = NRF_DRV_SPI_FREQ_1M;
  spi_config_epd.mode = NRF_DRV_SPI_MODE_0;
  spi_config_epd.bit_order = NRF_DRV_SPI_BIT_ORDER_MSB_FIRST;
  spi_config_epd.miso_pin = NRF_DRV_SPI_PIN_NOT_USED;
  spi_config_epd.mosi_pin = EPD_SPI_MOSI_PIN;
  spi_config_epd.sck_pin = EPD_SPI_SCK_PIN;
  spi_config_epd.ss_pin = EPD_SPI_CS_PIN;

  APP_ERROR_CHECK(nrf_drv_spi_init(&spi_epd, &spi_config_epd, NULL, NULL));
}
#endif

void EpdDigitalWriteCallback(int pin_num, int value) {
  if (value == HIGH) {
    // nrf_gpio_pin_set(pins[pin_num].pin);
    gpio_pin_write(epaper_gpio, pins[pin_num].pin, 1);
  } else {
    //nrf_gpio_pin_clear(pins[pin_num].pin);
    gpio_pin_write(epaper_gpio, pins[pin_num].pin, 0);
  }
}

int EpdDigitalReadCallback(int pin_num) {
  u32_t value;
  int read_status = gpio_pin_read(epaper_gpio, pins[pin_num].pin, &value);

  if(read_status)
  {
    printk("Failed to read gpio %d", pins[pin_num].pin);
    return -1;
  }

  if (value) {//nrf_gpio_pin_read(pins[pin_num].pin)) {
    return HIGH;
  } else {
    return LOW;
  }
}

void EpdDelayMsCallback(unsigned int delaytime) {
  // nrf_delay_ms(delaytime);
  k_sleep(delaytime);
}

void EpdSpiTransferCallback(unsigned char data)
{
  // nrf_gpio_pin_clear(pins[CS_PIN].pin);
  // nrf_drv_spi_transfer(&spi_epd, &data, 1, NULL, 0);
  // nrf_gpio_pin_set(pins[CS_PIN].pin);

  struct spi_buf tx_buf = { .buf = &data, .len = 1 };
	struct spi_buf_set tx_bufs = { .buffers = &tx_buf, .count = 1 };

  epaper_spi = device_get_binding("SPI_0");

  if(!epaper_spi)
  {
    printk("SPI device binding error\r\n");
    // return;
  }

  gpio_pin_write(epaper_gpio, pins[CS_PIN].pin, 0);

  epd_spi_cs_pin.gpio_dev = epaper_gpio;

  // int spi_status = spi_transceive(epaper_spi, &epaper_spi_config, &tx_bufs, NULL);

  int spi_status = spi_write(epaper_spi,&epaper_spi_config, &tx_bufs);

  if (spi_status < 0) {
    printk("SPI transceive error: %d\r\n", spi_status);
  }
  
  gpio_pin_write(epaper_gpio, pins[CS_PIN].pin, 1);
}

int EpdInitCallback(void) {
  pins[CS_PIN] = epd_cs_pin;
  pins[RST_PIN] = epd_rst_pin;
  pins[DC_PIN] = epd_dc_pin;
  pins[BUSY_PIN] = epd_busy_pin;
  
  return 0;
}

