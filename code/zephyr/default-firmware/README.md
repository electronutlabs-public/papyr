The papyr firmware is built on Zephyr rtos. This is the default firmware that ships with Papyr. The prebuilt binary (hex)  *papyr_v0.3.hex* can be found in the root directory inside *firmware* folder.

### Build and flash

To build the firmware follow the instructions below:

   1. Download Zephyr from this link -> https://github.com/zephyrproject-rtos/zephyr and follow their [getting started guide](https://docs.zephyrproject.org/latest/getting_started/getting_started.html).
   2. The latest version **v1.14** of Zephyr supports Papyr, but if you cannot find *nrf52840_papyr* inside *zephyr/boards/arm/*, then copy Papyr board files to **$ZEPHYR_BASE/boards/arm/**. The board files can be found inside *nrf52840_papyr* folder in the top level Papyr repo.
   3. From the `default-firmware` directory, run the following:

```
$ mkdir build && cd build

# Use cmake to configure a Ninja-based build system:
$ cmake -GNinja -DBOARD=nrf52840_papyr ..

# Now run ninja on the generated build system:
$ ninja

```
   4. Now connect any SWD programmer like [bumpy](https://docs.electronut.in/bumpy/)/black Magic Probe/JLink to *SWDIO* and *SWDCLK* pin of your papyr board. After connecting issue this command.


```
# Flash firmware
$ ninja flash
```

For more detailed instructions, please check [zephyr docs](https://docs.zephyrproject.org/latest/getting_started/getting_started.html#build-and-run-an-application)

