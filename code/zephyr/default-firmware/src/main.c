/**
 * @file main.c
 * @brief Papyr default firmware
 * 
 * @copyright Copyright (c) 2019, Electronut Labs (electronut.in)
 * 
 */

#include <zephyr/types.h>
#include <stddef.h>
#include <string.h>
#include <errno.h>
#include <misc/printk.h>
#include <misc/byteorder.h>
#include <zephyr.h>

#include <settings/settings.h>

#include <bluetooth/bluetooth.h>
#include <bluetooth/hci.h>
#include <bluetooth/conn.h>
#include <bluetooth/uuid.h>
#include <bluetooth/gatt.h>

#include <device.h>
#include <gpio.h>
#include <spi.h>

#include "epd1in54b.h"
#include "epdif.h"
#include "epdpaint.h"
#include "image.h"
#include "fonts.h"

#define PAPYR_ADV_CONN_PARAMS	BT_LE_ADV_PARAM(BT_LE_ADV_OPT_CONNECTABLE | \
											BT_LE_ADV_OPT_USE_NAME, \
											(500/0.625), \
											(600/0.625))

// e-Paper structure
EPD epd;

// Frame buffer to store image
unsigned char frame_buffer_black_arr[EPD_WIDTH * EPD_HEIGHT / 8];
unsigned char *frame_buffer_black = frame_buffer_black_arr;
unsigned char frame_buffer_red_arr[EPD_WIDTH * EPD_HEIGHT / 8];
unsigned char *frame_buffer_red = frame_buffer_red_arr;

// Paint structure
Paint paint_black;
Paint paint_red;

// Compressed and uncompressed buffers to store red and black image data
unsigned char uncompressed_red_bw_imageData[(((EPD_WIDTH * EPD_HEIGHT) / 8))*2];
unsigned char compressed_red_bw_imageData[(((EPD_WIDTH * EPD_HEIGHT) / 8) + 40)*2];

#define GREEN_LED 13
#define BLUE_LED 14
#define RED_LED 15
#define RESET_PIN 18
#define E_INK 11
#define P0_08 8

// e-Paper functions initialization
int epaper_init();
void epaper_paint_init();
void epaper_default_image();
void epaper_clear();

// PORT0 instance
struct device * port0;

// Turn off RGB LED
void leds_off()
{
	gpio_pin_write(port0, GREEN_LED, 1);
    gpio_pin_write(port0, BLUE_LED, 1);
    gpio_pin_write(port0, RED_LED, 1);
}

// Pull down epaper pins to save power
void epaper_pins_off()
{
    gpio_pin_configure(port0, EPD_BUSY_PIN, GPIO_DIR_OUT);
	gpio_pin_configure(port0, EPD_RST_PIN, GPIO_DIR_OUT);
	gpio_pin_configure(port0, EPD_DC_PIN, GPIO_DIR_OUT);
	gpio_pin_configure(port0, EPD_SPI_CS_PIN, GPIO_DIR_OUT);
	gpio_pin_configure(port0, EPD_SPI_SCK_PIN, GPIO_DIR_OUT);
	gpio_pin_configure(port0, EPD_SPI_MOSI_PIN, GPIO_DIR_OUT);

    gpio_pin_write(port0, EPD_BUSY_PIN, 0);
	gpio_pin_write(port0, EPD_RST_PIN, 0);
	gpio_pin_write(port0, EPD_DC_PIN, 0);
	gpio_pin_write(port0, EPD_SPI_CS_PIN, 0);
	gpio_pin_write(port0, EPD_SPI_SCK_PIN, 0);
	gpio_pin_write(port0, EPD_SPI_MOSI_PIN, 0);
}

/* Custom Service Variables */
static struct bt_uuid_128 vnd_uuid = BT_UUID_INIT_128(
	0x13, 0x81, 0x10, 0x46, 0xef, 0x92, 0xb0, 0x9f,
	0xe8, 0x4d, 0xb7, 0x32, 0x00, 0x14, 0x3a, 0x7f);

static struct bt_uuid_128 vnd_enc_uuid = BT_UUID_INIT_128(
	0x13, 0x81, 0x10, 0x46, 0xef, 0x92, 0xb0, 0x9f,
	0xe8, 0x4d, 0xb7, 0x32, 0x01, 0x14, 0x3a, 0x7f);

static u8_t vnd_value[18] = {'0'};

// BLE characteristic read handler
static ssize_t read_vnd(struct bt_conn *conn, const struct bt_gatt_attr *attr,
			void *buf, u16_t len, u16_t offset)
{
	const char *value = attr->user_data;

	printk("value read : %s \n", value);

	return bt_gatt_attr_read(conn, attr, buf, len, offset, value,
				 strlen(value));
}

/*
	Uncompress a RLE buffer
	This is of little use since the size of the output buffer
	isn't known. However this can be used as the basis for ones
	own decoder.
*/
void RLEUncompress(unsigned char *output,const unsigned char *input,int length)
{
   signed char count;

   while (length > 0) {
      count = (signed char)*input++;
      if (count > 0) {                      /* replicate run */
         memset(output,*input++,count);
      } else if (count < 0) {               /* literal run */
         count = (signed char)-count;
         memcpy(output,input,count);
         input += count;
      } /* if */
      output += count;
      length -= count;
   } /* while */
}

// Flag to check if complete image is received or not, so image can be displayed
volatile bool full_image_received = false;

// BLE characteristic write handler
static ssize_t write_vnd(struct bt_conn *conn, const struct bt_gatt_attr *attr,
			 const void *buf, u16_t len, u16_t offset,
			 u8_t flags)
{
	u8_t *value = attr->user_data;

	if (offset + len > sizeof(vnd_value)) {
		printk("write error\n");
		return BT_GATT_ERR(BT_ATT_ERR_INVALID_OFFSET);
	}

	memcpy(value + offset, buf, len);

	int data_length = len-2;
	static int frame_no;
	int tmp_frame_no = ((value[0]<<8) | value[1]);
	bool last_frame = false;

	static u32_t red_led_state = 1;

	if(tmp_frame_no==0) // end frame
	{
		frame_no++;
		last_frame = true;
		printk("last frame\n");

		printk("%d : ", frame_no);

		for(int i=0; i<len; i++)
		{
			printk("%02x", value[i]);
		}
		printk("\n");

		for (int i = 0; i < data_length; i++) {
			// Store image data frames received into compressed_red_bw_imageData
			compressed_red_bw_imageData[((frame_no-1)*16)+i] = value[i+2];
		}
		
		leds_off();
		gpio_pin_write(port0, GREEN_LED, 0);
		full_image_received = true;
	}
	else
	{
		frame_no = tmp_frame_no;
		printk("%d : ", frame_no);

		for(int i=0; i<len; i++)
		{
			printk("%02x", value[i]);
		}
		printk("\n");

		for (int i = 0; i < data_length; i++) {
			// Store image data frames received into compressed_red_bw_imageData
			compressed_red_bw_imageData[((frame_no-1)*data_length)+i] = value[i+2];
		}

		leds_off();
		red_led_state = red_led_state ? 0:1;

    	gpio_pin_write(port0, RED_LED, red_led_state);
	}

	return len;
}

/* Vendor Primary Service Declaration */
static struct bt_gatt_attr vnd_attrs[] = {
	/* Vendor Primary Service Declaration */
	BT_GATT_PRIMARY_SERVICE(&vnd_uuid),
	BT_GATT_CHARACTERISTIC(&vnd_enc_uuid.uuid,
			       BT_GATT_CHRC_READ | BT_GATT_CHRC_WRITE,
			       BT_GATT_PERM_READ | BT_GATT_PERM_WRITE,
			       read_vnd, write_vnd, vnd_value),
};

static struct bt_gatt_service vnd_svc = BT_GATT_SERVICE(vnd_attrs);

static const struct bt_data ad[] = {
	BT_DATA_BYTES(BT_DATA_FLAGS, (BT_LE_AD_GENERAL | BT_LE_AD_NO_BREDR)),
};

// BLE connect handler
static void connected(struct bt_conn *conn, u8_t err)
{
	if (err) {
		printk("Connection failed (err %u)\n", err);
	} else {
		printk("Connected\n");

		leds_off();

    	gpio_pin_write(port0, GREEN_LED, 0);
	}
}

// BLE disconnect handler
static void disconnected(struct bt_conn *conn, u8_t reason)
{
	printk("Disconnected (reason %u)\n", reason);
	leds_off();
}

static struct bt_conn_cb conn_callbacks = {
	.connected = connected,
	.disconnected = disconnected,
};

// Callback on ble enable
static void bt_ready(int err)
{
	if (err) {
		printk("Bluetooth init failed (err %d)\n", err);
		return;
	}

	printk("Bluetooth initialized\n");

	bt_gatt_service_register(&vnd_svc);

	if (IS_ENABLED(CONFIG_SETTINGS)) {
		settings_load();
	}

	err = bt_le_adv_start(PAPYR_ADV_CONN_PARAMS, ad, ARRAY_SIZE(ad), NULL, 0);
	if (err) {
		printk("Advertising failed to start (err %d)\n", err);
		return;
	}

	printk("Advertising successfully started\n");
}

/**************************EPAPER DISPLAY**************************/
// Initialize e-Paper display
int epaper_init()
{
    epaper_GPIO_Init();

    return EPD_Init(&epd);
}

// Initialize e-Paper paint/frame structure
void epaper_paint_init()
{
    Paint_Init(&paint_black, frame_buffer_black, epd.width, epd.height);
    Paint_Init(&paint_red, frame_buffer_red, epd.width, epd.height);
    Paint_Clear(&paint_black, UNCOLORED);
    Paint_Clear(&paint_red, UNCOLORED);
}

// Display default image
void epaper_default_image()
{
	Paint_SetRotate(&paint_red, 3);
    Paint_DrawStringAt(&paint_red, 23, 40, "p a p y r", &Font24, COLORED);
    Paint_DrawBitmap(&paint_black, 100, 0, bwData, image_width, image_height, COLORED);

    EPD_DisplayFrame(&epd, frame_buffer_black, frame_buffer_red);

	volatile u32_t flag = 1;

	// set flag in uicr for not displaying default image after every reset
	NRF_NVMC->CONFIG = NVMC_CONFIG_WEN_Wen << NVMC_CONFIG_WEN_Pos;
	while (NRF_NVMC->READY == NVMC_READY_READY_Busy){}
	*(uint32_t *)0x10001080 = flag;
	NRF_NVMC->CONFIG = NVMC_CONFIG_WEN_Ren << NVMC_CONFIG_WEN_Pos;
	while (NRF_NVMC->READY == NVMC_READY_READY_Busy){}
}

// Clear e-Paper display
void epaper_clear()
{
	epaper_paint_init();
	EPD_DisplayFrame(&epd, frame_buffer_black, frame_buffer_red);
}
/**************************EPAPER DISPLAY**************************/

void main(void)
{
	printk("Starting...");

	int err;
	// Enable BLE
	err = bt_enable(bt_ready);
	if (err) {
		printk("Bluetooth init failed (err %d)\n", err);
		return;
	}

	bt_conn_cb_register(&conn_callbacks);

	port0 = device_get_binding("GPIO_0");

	/* Set LED pin as output */
    gpio_pin_configure(port0, GREEN_LED, GPIO_DIR_OUT);
	gpio_pin_configure(port0, BLUE_LED, GPIO_DIR_OUT);
    gpio_pin_configure(port0, RED_LED, GPIO_DIR_OUT);
	leds_off();

	gpio_pin_write(port0, GREEN_LED, 0);
	k_sleep(200);
	gpio_pin_write(port0, GREEN_LED, 1);
	k_sleep(200);
	gpio_pin_write(port0, GREEN_LED, 0);
	k_sleep(200);
	gpio_pin_write(port0, GREEN_LED, 1);

    // turn off epaper display MOSFET
    gpio_pin_configure(port0, E_INK, GPIO_DIR_OUT);
	gpio_pin_write(port0, E_INK, 1);

	// Turn off e-Paper pins
	epaper_pins_off();

	k_sleep(200);

	// Read UICR register CUSTOMER[0]/0x10001080
	u32_t flag;
    memcpy(&flag, (uint8_t *)0x10001080, sizeof(flag));
	printk("\n%d\n", flag);

	// Read GPIO P0.08 pin
	gpio_pin_configure(port0, P0_08, GPIO_DIR_IN | GPIO_PUD_PULL_DOWN);
	u32_t p0_08_state;
	gpio_pin_read(port0, P0_08, &p0_08_state);
	printk("\np0_08_state: %d\n", p0_08_state);

	// if flag is not set in uicr register or GPIO P0.08 is high then display default image
	if((flag != 1) || (p0_08_state == 1))
	{
		printk("\nDisplaying default image...\n");
		// turn on epaper display MOSFET
		gpio_pin_write(port0, E_INK, 0);

		// init epaper display
		epaper_init();

		epaper_paint_init();
		epaper_default_image();
		// turn off epaper display MOSFET
		gpio_pin_write(port0, E_INK, 1);
		epaper_pins_off();
		printk("done.");
	}

	while (1) {
		k_sleep(MSEC_PER_SEC);

        // Draw image received over BLE
		if(full_image_received)
		{
			printk("painting...\n");

			// Decompress received image data
			RLEUncompress(uncompressed_red_bw_imageData, compressed_red_bw_imageData, sizeof(uncompressed_red_bw_imageData));

			// Turn off display MOSFET
			gpio_pin_write(port0, E_INK, 1);
			epaper_pins_off();
			k_sleep(300);
			// Turn on display MOSFET
			gpio_pin_write(port0, E_INK, 0);

			// Initialize display
			epaper_init();
			epaper_paint_init();
			
			// Clear display
			Paint_Clear(&paint_black, UNCOLORED);
			Paint_Clear(&paint_red, UNCOLORED);

			// Fill buffer with decompressed black pixels
			Paint_DrawBitmap(&paint_black, 0, 0, uncompressed_red_bw_imageData, EPD_WIDTH, EPD_HEIGHT, COLORED);
			// Fill buffer with decompressed red pixels
			Paint_DrawBitmap(&paint_red, 0, 0, (uncompressed_red_bw_imageData+5000), EPD_WIDTH, EPD_HEIGHT, COLORED);
			// Display image
			EPD_DisplayFrame(&epd, frame_buffer_black, frame_buffer_red);

			epaper_pins_off();
			gpio_pin_write(port0, E_INK, 1);
			full_image_received = false;
		}
	}
}
