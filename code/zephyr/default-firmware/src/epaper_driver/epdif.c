/**
 *  @filename   :   epdif.c
 *  @brief      :   Implements EPD interface functions
 *                  Users have to implement all the functions in epdif.cpp
 *  @author     :   Yehui from Waveshare
 *
 *  Copyright (C) Waveshare     July 7 2017
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documnetation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to  whom the Software is
 * furished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS OR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <zephyr.h>
#include <device.h>
#include <gpio.h>
#include <spi.h>
#include "epdif.h"

// GPIO
struct device *epaper_gpio;

// SPI
struct device *epaper_spi;

struct spi_cs_control epd_spi_cs_pin = {
	.gpio_pin = EPD_SPI_CS_PIN,
	.delay = 0,
};

struct spi_config epaper_spi_config = {
  .frequency = 268435456, // FAST_FREQ
  .operation = SPI_WORD_SET(8), // SPI_MODE_CPOL, SPI_MODE_CPHA, SPI_OP_MODE_MASTER
  .cs = &epd_spi_cs_pin,
};

#include "epdif.h"

EPD_Pin epd_cs_pin = {
  EPD_SPI_CS_PIN,
};

EPD_Pin epd_rst_pin = {
  EPD_RST_PIN,
};

EPD_Pin epd_dc_pin = {
  EPD_DC_PIN,
};

EPD_Pin epd_busy_pin = {
  EPD_BUSY_PIN,
};

EPD_Pin pins[4];

void epaper_GPIO_Init(void)
{
  epaper_gpio = device_get_binding("GPIO_0");

  if(!epaper_gpio)
  {
    printk("GPIO binding error\r\n");
  }

  gpio_pin_configure(epaper_gpio, EPD_DC_PIN, GPIO_DIR_OUT);
  gpio_pin_configure(epaper_gpio, EPD_RST_PIN, GPIO_DIR_OUT);
  gpio_pin_configure(epaper_gpio, EPD_SPI_CS_PIN, GPIO_DIR_OUT);
  gpio_pin_configure(epaper_gpio, EPD_BUSY_PIN, GPIO_DIR_IN);

  gpio_pin_write(epaper_gpio, EPD_DC_PIN, 0);
  gpio_pin_write(epaper_gpio, EPD_RST_PIN, 0);
  gpio_pin_write(epaper_gpio, EPD_SPI_CS_PIN, 0);
}

void EpdDigitalWriteCallback(int pin_num, int value) {
  if (value == HIGH) {
    gpio_pin_write(epaper_gpio, pins[pin_num].pin, 1);
  } else {
    gpio_pin_write(epaper_gpio, pins[pin_num].pin, 0);
  }
}

int EpdDigitalReadCallback(int pin_num) {
  u32_t value;
  int read_status = gpio_pin_read(epaper_gpio, pins[pin_num].pin, &value);

  if(read_status)
  {
    printk("Failed to read gpio %d", pins[pin_num].pin);
    return -1;
  }

  if (value) {//nrf_gpio_pin_read(pins[pin_num].pin)) {
    return HIGH;
  } else {
    return LOW;
  }
}

void EpdDelayMsCallback(unsigned int delaytime) {
  k_sleep(delaytime);
}

void EpdSpiTransferCallback(unsigned char data)
{
  struct spi_buf tx_buf = { .buf = &data, .len = 1 };
	struct spi_buf_set tx_bufs = { .buffers = &tx_buf, .count = 1 };

  epaper_spi = device_get_binding("SPI_1");

  if(!epaper_spi)
  {
    printk("SPI device binding error\r\n");
    // return;
  }

  gpio_pin_write(epaper_gpio, pins[CS_PIN].pin, 0);

  epd_spi_cs_pin.gpio_dev = epaper_gpio;

  int spi_status = spi_write(epaper_spi,&epaper_spi_config, &tx_bufs);

  if (spi_status < 0) {
    printk("SPI transceive error: %d\r\n", spi_status);
  }
  
  gpio_pin_write(epaper_gpio, pins[CS_PIN].pin, 1);
}

int EpdInitCallback(void) {
  pins[CS_PIN] = epd_cs_pin;
  pins[RST_PIN] = epd_rst_pin;
  pins[DC_PIN] = epd_dc_pin;
  pins[BUSY_PIN] = epd_busy_pin;
  
  return 0;
}

