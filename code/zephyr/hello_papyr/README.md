The papyr firmware is built on Zephyr rtos. 

## Hello Papyr Example

In this example you will see "Hello papyr !" displayed on epaper.

![papyr_image](hello_papyr.jpg)


### Build and flash

To build the firmware follow the instructions below:

Hope you have zephyr setup on your local, if not follow instructions [here](../).  

```
$ mkdir build && cd build

# Use cmake to configure a Ninja-based build system:
$ cmake -GNinja -DBOARD=nrf52840_papyr ..

# Now run ninja on the generated build system:
$ ninja

```
   1. Now connect any SWD programmer like [bumpy](https://docs.electronut.in/bumpy/)/black Magic Probe/JLink to *SWDIO* and *SWDCLK* pin of your papyr board. After connecting issue this command.


```
# Flash firmware
$ ninja flash
```