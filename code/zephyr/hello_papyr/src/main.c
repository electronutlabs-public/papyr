/** @file main.c
 *
 * @brief Hello_Papyr test code with zephyr rtos. 
 *
 * @par
 * COPYRIGHT NOTICE: (c) 2018 Electronut Labs.
 * All rights reserved. 
*/

#include <zephyr.h>
#include <misc/printk.h>

#include <device.h>
#include <gpio.h>
#include <spi.h>
#include "epaper.h"

struct device *port0 = NULL;

/**
 *  @brief: turn on epaper
 */
void epaper_turnOn(void)
{
    // turn on epaper display MOSFET
    gpio_pin_configure(port0, 11, GPIO_DIR_OUT);
    gpio_pin_write(port0, 11, 0);
}
/**
 *  @brief: initialise led and blink green
 */
void led_init(void)
{
    /* Set LED pin as output */
    gpio_pin_configure(port0, 13, GPIO_DIR_OUT);
    gpio_pin_configure(port0, 14, GPIO_DIR_OUT);
    gpio_pin_configure(port0, 15, GPIO_DIR_OUT);

    gpio_pin_write(port0, 13, 1);
    gpio_pin_write(port0, 14, 1);
    gpio_pin_write(port0, 15, 1);

    gpio_pin_write(port0, 13, 0);
    k_sleep(500);
    gpio_pin_write(port0, 13, 1);
    k_sleep(500);
}
/**
 *  @brief: main
 */
void main(void)
{
    // print on serial
    printk("Hello papyr! %s\n", CONFIG_BOARD);

    port0 = device_get_binding("GPIO_0");

    epaper_turnOn();
    led_init();

    //init epaper display
    epaper_init();
    epaper_test();

    for (;;)
    {
        k_sleep(2500);
    }
}
