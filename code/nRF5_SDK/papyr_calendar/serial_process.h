#ifndef SERIAL_PROCESS_H
#define	SERIAL_PROCESS_H

#ifdef	__cplusplus
extern "C" {
#endif

#include <time.h>
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

#include "app_error.h"
#include "app_util.h"
#include "app_usbd_core.h"
#include "app_usbd.h"
#include "app_usbd_string_desc.h"
#include "app_usbd_cdc_acm.h"
#include "app_usbd_serial_num.h"

#define GREEN_LED 13
#define BLUE_LED 14
#define RED_LED 15

#define LED_CDC_ACM_OPEN    GREEN_LED
#define LED_CDC_ACM_RX      RED_LED
#define LED_CDC_ACM_TX      BLUE_LED

#define READ_SIZE 1

/**
 * @brief User event handler @ref app_usbd_cdc_acm_user_ev_handler_t
 * */
void cdc_acm_user_ev_handler(app_usbd_class_inst_t const * p_inst,
                                    app_usbd_cdc_acm_user_event_t event);

void usbd_user_ev_handler(app_usbd_event_type_t event);

void print_current_time();

#define CDC_ACM_COMM_INTERFACE  0
#define CDC_ACM_COMM_EPIN       NRF_DRV_USBD_EPIN2

#define CDC_ACM_DATA_INTERFACE  1
#define CDC_ACM_DATA_EPIN       NRF_DRV_USBD_EPIN1
#define CDC_ACM_DATA_EPOUT      NRF_DRV_USBD_EPOUT1

/**
 * @brief Enable power USB detection
 *
 * Configure if example supports USB port connection
 */
#ifndef USBD_POWER_DETECTION
#define USBD_POWER_DETECTION false
#endif

// Process calendar time set/get commands
void serial_process(void);

// Initialize USBD peripheral
void usbd_init(void);

#ifdef	__cplusplus
}
#endif

#endif	/* SERIAL_PROCESS_H */
