## Build and flash firmware

Set up development environment for nRF5 15.2 SDK from [here](https://www.nordicsemi.com/DocLib/Content/SDK_Doc/nRF5_SDK/v15-2-0/index). Once you are done installing toolchain, SDK, GDB and other tools depending upon your system (Linux or Windows) you can run the examples inside *nrf5_SDK* in Papyr repo.

**Build and flash Hello Papyr! example with nRF5 SDK**

1. Copy the example to *nRF5_SDK_15.2.0_9412b96/examples/* folder.
2. Open terminal and navigate to *pca10056/blank/armgcc/*
3. Issue *make* command to compile the example.

```
make
```
4. You can flash the firmware either using nRF52840-DK or using bumpy. Here are the steps for both.

**Upload firmware with nRF52840-DK**

| DK | PAPYR |
|----------|-------|
| VTG | VDD|
| GND DETECT | GND|
| SWCLK | SWCLK|
| SWDIO | SWDIO|

then upload the code using the below command.
```
make flash
```
If done as directed, you will see **Hello Papyr!** message displayed on papyr board. 

**Upload firmware with bumpy**

Connect bumpy and PogoProg as directed in programming with Zephyr section above and append below code snippet to your Makefile.

```
# specify your comm port
$export BMP_PORT /dev/ttyACMx

# Flash softdevice
flash_softdevice_bmp:
    @echo Flashing: s140_nrf52_6.1.0_softdevice.hex
    arm-none-eabi-gdb -q -ex 'target extended-remote $(BMP_PORT)' -ex 'monitor swdp_scan' -ex 'attach 1' -ex 'load' -ex 'mon hard_srst' -ex 'detach' -ex 'quit' -ex 'kill' $(SDK_ROOT)/components/softdevice/s140/hex/s140_nrf52_6.1.0_softdevice.hex

# Flash the program
flash_bmp: $(OUTPUT_DIRECTORY)/nrf52840_xxaa.out
    @echo Flashing: $<
    arm-none-eabi-gdb -ex 'target extended-remote $(BMP_PORT)' -ex 'monitor swdp_scan' -ex 'attach 1' -ex 'load' -ex 'mon hard_srst' -ex 'set remote exec-file $<' -ex 'run' -ex 'quit' -ex 'kill' $<

erase_bmp:
    arm-none-eabi-gdb -q -ex 'target extended-remote $(BMP_PORT)' -ex 'monitor swdp_scan' -ex 'attach 1' -ex 'mon erase_mass' -ex 'mon hard_srst' -ex 'detach' -ex 'quit'
```
To flash the firmware, run command. 

```
 make flash_bmp
```
If the flash is successful, you will see **Hello Papyr !** displayed on your board. 