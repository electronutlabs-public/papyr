// base class GxEPD2_GFX can be used to pass references or pointers to the display instance as parameter, uses ~1.2k more code
// enable or disable GxEPD2_GFX base class
#define ENABLE_GxEPD2_GFX 0

#include <GxEPD2_3C.h>
#include <Fonts/FreeMonoBold9pt7b.h>

/**
 * E-Paper connections
 * BUSY - 3
 * RST  - 2
 * DC   - 28
 * CS   - 30
 * SCK  - 31
 * MOSI - 29
 * 
 * MOSFET - 11 // display has one mosfet for turning on and off the e-paper display
 */

#define MAX_DISPAY_BUFFER_SIZE 15000ul // ~15k is a good compromise
#define MAX_HEIGHT_3C(EPD) (EPD::HEIGHT <= (MAX_DISPAY_BUFFER_SIZE / 2) / (EPD::WIDTH / 8) ? EPD::HEIGHT : (MAX_DISPAY_BUFFER_SIZE / 2) / (EPD::WIDTH / 8))
GxEPD2_3C<GxEPD2_154c, MAX_HEIGHT_3C(GxEPD2_154c)> display(GxEPD2_154c(/*CS*/ 30, /*DC*/ 28, /*RST*/ 2, /*BUSY*/ 3));

#include "bitmaps/Bitmaps3c200x200.h"

#define E_PAPER_MOSFET 11 // mosfet to turn on/off the display

void setup()
{
  // turn on e-paper display
  pinMode(E_PAPER_MOSFET, OUTPUT);
  digitalWrite(E_PAPER_MOSFET, LOW);
  
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, LOW);
  delay(500); 
  digitalWrite(LED_BUILTIN, HIGH);
  delay(500); 
  digitalWrite(LED_BUILTIN, LOW);
  
  Serial1.begin(115200);
  Serial1.println();
  Serial1.println("setup");
  delay(100);
  display.init(115200);

  helloWorld();
  delay(1000);

  showFont("FreeMonoBold9pt7b", &FreeMonoBold9pt7b);
  delay(1000);

  drawBitmaps3c200x200();
  delay(1000);

  drawGeometries();
  delay(1000);

  Serial1.println("setup done");
}

void loop()
{
}

const char HelloWorld[] = "Hello World!";

void helloWorld()
{
  //Serial1.println("helloWorld");
  display.setRotation(3);
  display.setFont(&FreeMonoBold9pt7b);
  display.setTextColor(GxEPD_BLACK);
  int16_t tbx, tby; uint16_t tbw, tbh;
  display.getTextBounds(HelloWorld, 0, 0, &tbx, &tby, &tbw, &tbh);
  // center bounding box by transposition of origin:
  uint16_t x = ((display.width() - tbw) / 2) - tbx;
  uint16_t y = ((display.height() - tbh) / 2) - tby;
  display.setFullWindow();
  display.firstPage();
  do
  {
    display.fillScreen(GxEPD_WHITE);
    display.setCursor(x, y);
    display.print(HelloWorld);
  }
  while (display.nextPage());
  //Serial1.println("helloWorld done");
}

void drawGeometries()
{
  //Serial1.println("drawGeometries");
  display.setRotation(3);
  display.setFullWindow();
  display.firstPage();
  do
  {
    display.fillScreen(GxEPD_WHITE);
    display.fillRect(20, 10, 60, 50, GxEPD_BLACK);
    display.fillCircle(100, 100, 30, GxEPD_RED);
    display.fillTriangle(115, 65, 150, 5, 185, 65, GxEPD_RED);
    display.fillTriangle(120, 60, 150, 20, 180, 60, GxEPD_WHITE);
    display.drawRoundRect(10, 130, 50, 50, 10, GxEPD_RED);
    display.drawRoundRect(13, 133, 44, 44, 10, GxEPD_BLACK);
    display.drawLine(190, 130, 120, 170, GxEPD_RED);
    display.drawLine(190, 131, 120, 171, GxEPD_RED);
    display.drawLine(190, 132, 120, 172, GxEPD_RED);
    display.drawLine(190, 133, 120, 173, GxEPD_RED);
  }
  while (display.nextPage());
  //Serial1.println("drawGeometries");
}

void showFont(const char name[], const GFXfont* f)
{
  display.setFullWindow();
  display.setRotation(3);
  display.setTextColor(GxEPD_BLACK);
  display.firstPage();
  do
  {
    drawFont(name, f);
  }
  while (display.nextPage());
}

void drawFont(const char name[], const GFXfont* f)
{
  display.fillScreen(GxEPD_WHITE);
  display.setTextColor(GxEPD_BLACK);
  display.setFont(f);
  display.setCursor(0, 0);
  display.println();
  display.println(name);
  display.println(" !\"#$%&'()*+,-./");
  display.println("0123456789:;<=>?");
  display.println("@ABCDEFGHIJKLMNO");
  display.println("PQRSTUVWXYZ[\\]^_");
  if (display.epd2.hasColor)
  {
    display.setTextColor(GxEPD_RED);
  }
  display.println("`abcdefghijklmno");
  display.println("pqrstuvwxyz{|}~ ");
}

struct bitmap_pair
{
  const unsigned char* black;
  const unsigned char* red;
};

void drawBitmaps3c200x200()
{
  bitmap_pair bitmap_pairs[] =
  {
    {WS_Bitmap3c200x200_black, WS_Bitmap3c200x200_red}
  };
  
  // display.clearScreen();
  display.drawImage(bitmap_pairs[0].black, bitmap_pairs[0].red, 0, 0, 200, 200, false, false, true);
}
