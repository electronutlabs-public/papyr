# 
# Weather Display on Papyr nRF52840 epaper display 
#
# Code by Chirag Rathod (chirag.rathod@gmail.com)
# 

# MQTT specifix library
import paho.mqtt.client as mqtt # Installed via: pip install paho-mqtt

# Rendering specific library
import svgwrite # Installed via: pip install svgwrite

# Data specific libraries
import json
import requests

# Broker and Topic Deta
MQTT_BROKER_HOST_ADDRESS = 'broker.hivemq.com'
MQTT_BROKER_HOST_PORT = 1883
MQTT_TOPIC = 'electronut/MJEOU/svg' # IMP: Update this before running


def getData():
    # Get your location
    response = requests.get('http://ipinfo.io/json')
    locData = response.json()
    city = locData['city']
    country = locData['country']
    cityCountry = city + ',' + country

    # Use location to get weather information
    # Please generate and use your own API key
    weatherUrl = 'http://api.openweathermap.org/data/2.5/weather?q=' + cityCountry + '&APPID=fb95afea4ef5a316b89a5df739be9b10'

    response = requests.get(weatherUrl)
    data = response.json()
    place = data['name']
    weather = data['weather'][0]['main']
    return [place, weather]

def getSVGPayload():
    data = getData()

    # Create a drawing object
    dwg = svgwrite.Drawing('svgwrite-sample.svg', profile='tiny', viewBox=('0 0 200 200'))

    # Draw a black box
    dwg.add(dwg.rect((0, 0), (200, 200), fill='black'))

    # Draw the place
    dwg.add(dwg.text(data[0],
        insert=(5, 90),
        fill='white',
        stroke_width=2,
        font_size='25px',
        font_weight="bold",
        font_family="Courier New")
    )

    # Draw the weather
    dwg.add(dwg.text(data[1],
        insert=(5, 130),
        fill='white',
        stroke_width=2,
        font_size='45px',
        font_weight="bold",
        font_family="Courier New")
    )

    # String will be sent as payload
    return dwg.tostring()


# Create client and connect to it
client = mqtt.Client()
client.connect(MQTT_BROKER_HOST_ADDRESS, MQTT_BROKER_HOST_PORT)

# Generate payload in SVG format
payload = getSVGPayload()

# Publish data from broker to clients
client.publish(MQTT_TOPIC, payload)
